from datetime import datetime
import os

from twisted.internet.defer import inlineCallbacks, returnValue
from twisted.internet import inotify
from twisted.python import filepath
from twisted.internet.utils import getProcessOutput

from base import BaseBotFactory, BaseBotProtocol
import messages

WATCH_MASK = inotify.IN_CREATE | inotify.IN_MOVED_TO


def result_name(filepath):
    """Given a filepath, return formatted resulting directory name, according to
    rtname"""
    rtname_result = getProcessOutput("rtname", [filepath.path])
    return rtname_result.addCallback(lambda r: r.strip())

class SpambotProtocol(BaseBotProtocol):
    def signedOn(self):
        super(SpambotProtocol, self).signedOn()
        self.notifier = inotify.INotify()
        self.notifier.startReading()
        for directory in self.factory.download:
            self.notifier.watch(filepath.FilePath(directory),
                callbacks=[self.download])

        for directory in self.factory.watch:
            self.notifier.watch(filepath.FilePath(directory),
                callbacks=[self.watch])

        for directory in self.factory.completed:
            self.notifier.watch(filepath.FilePath(directory),
                callbacks=[self.completed])

    @inlineCallbacks
    def _get_and_update_db(self, filename, column):
        if column not in {'download', 'watch', 'completed'}:
            raise ValueError("Column must be download, watch or completed")

        stored = yield self.factory.dbpool.runQuery("""select watch,
            download, completed from torty where filename=?""", (filename, ))

        if stored:
            stored = [datetime.strptime(d, "%Y-%m-%d %H:%M:%S") if d is not None else None
                for d in stored[0]]
            sqlcommand = """update torty set %s=DateTime('now') where
                filename=?""" % (column, )
        else:
            stored = [None] * 3
            sqlcommand = """insert into torty (filename, %s) values
            (?, DateTime('now'))""" % (column, )

        yield self.factory.dbpool.runQuery(sqlcommand, (filename, ))
        returnValue(stored)

    @inlineCallbacks
    def download(self, ignored, filepath, mask):
        if mask & WATCH_MASK:
            filename = yield result_name(filepath)
            stored = yield self._get_and_update_db(filename, column='download')
            message = messages.download_message(filename=filename, filepath=filepath,
                watch=stored[0], download=datetime.now())
            self.say(self.factory.watch_channel, message)

    @inlineCallbacks
    def completed(self, ignored, filepath, mask):
        if mask & WATCH_MASK:
            filename = yield result_name(filepath)
            stored = yield self._get_and_update_db(filename, column='completed')
            message = messages.completed_message(filename=filename, filepath=filepath,
                watch=stored[0], download=stored[1], completed=datetime.now())
            self.say(self.factory.watch_channel, message)

    @inlineCallbacks
    def watch(self, ignored, filepath, mask):
        if mask & WATCH_MASK:
            filename = yield result_name(filepath)
            stored = yield self._get_and_update_db(filename, column='watch')
            message = messages.watch_message(filename=filename, filepath=filepath,
                watch=datetime.now())
            self.say(self.factory.watch_channel, message)

    def command_SEARCH(self, channel, nick, words):
        found = []
        for download_dir in self.factory.download:
            for filename in os.listdir(download_dir):
                if all(word.lower() in filename.lower() for word in words):
                    found.append(filename)

        self.say_pastebin(channel, found, message="%s: %%s" % (nick, ))

class SpambotFactory(BaseBotFactory):
    protocol = SpambotProtocol

    def startFactory(self):
        BaseBotFactory.startFactory(self)
        self.dbpool.runQuery("""
            create table if not exists torty (
                filename string,
                watch datetime,
                download datetime,
                completed datetime
            )""")
