def download_message(filename, filepath, watch, download):
    parent_dir = filepath.parent().parent().basename() 
    return "%s Downloading %s/%s (watched on %s)" % (download, filename, 
        parent_dir, watch)

def completed_message(filename, filepath, watch, download, completed):
    parent_dir = filepath.parent().parent().basename()
    return "%s Completed %s/%s (watched on %s, started on %s)" % (download, 
        filename, parent_dir, watch, download)

def watch_message(filename, filepath, watch):
    parent_dir = filepath.parent().parent().basename()
    return "%s Watching %s/%s " % (watch, filename, parent_dir)
