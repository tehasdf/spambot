import json

from zope.interface import implements

from twisted.python import usage
from twisted.plugin import IPlugin
from twisted.application.service import IServiceMaker
from twisted.application import internet

from spambotproto import SpambotFactory


class Options(usage.Options):
    def __init__(self):
        usage.Options.__init__(self)
        self['watch'] = []
        self['download'] = []
        self['completed'] = []
        self['channels'] = []

    optParameters = [
        ['HOST', 'h', 'irc.rizon.net', 'IRC server to connect'],
        ['PORT', 'p', 6667, 'Port to connect to the irc server on', int],
        ['nickname', 'n', 'spambot', 'Nick'],
        ['password', 'w', None, 'Server password'],
        ['dbname', 'd', 'database.sqlite3', 'Database to use'],
        ['filename', 'f', None, "Config file that overrides commandline options"]
    ]

    def opt_channel(self, dir):
            self['channels'].append(dir)

    opt_c = opt_channel

    def opt_download(self, dir):
        self['download'].append(dir)

    def opt_watch(self, dir):
            self['watch'].append(dir)

    def opt_completed(self, dir):
            self['completed'].append(dir)

    opt_D = opt_download
    opt_W = opt_watch
    opt_C = opt_completed

    def postOptions(self):
        if self['filename'] is not None:
            with open(self['filename']) as f:
                file_config = json.load(f)
            for k, v in file_config.iteritems():
                if isinstance(v, unicode):
                    v = v.encode('utf-8')
                self[k] = v


class BotServiceMaker(object):
    implements(IServiceMaker, IPlugin)
    tapname = "spambot"
    description = "spambot"
    options = Options

    def makeService(self, options):
        return internet.TCPClient(options['HOST'], options['PORT'],
            SpambotFactory(**options))


serviceMaker = BotServiceMaker()
